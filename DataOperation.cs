﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace greengrocer
{
    public class DataOperation : IDisposable
    {
        private readonly GreengrocerContext context = new GreengrocerContext();
        public DataOperation(GreengrocerContext context)
        {
            this.context = context;
        }

        public void Dispose()
        {
            context.Dispose();
        }

        //データの挿入
        public void AddProduct(int id, string name, int price, int quantity)
        {
            Product newProduct = new Product
            {
                id = id,
                name = name,
                price = price,
                quantity = quantity
            };
            context.Products.Add(newProduct);
            context.SaveChanges();
        }

        //データの削除
        public void RemoveProduct(int id)
        {
            Product product = context.Products.FirstOrDefault(x => x.id == id);
            if (product != null)
            {
                context.Products.Remove(product);
                context.SaveChanges();
            }
        }

        //データの更新
        public void UpdateProductToHalfPrice(int id)
        {
            Product product = context.Products.FirstOrDefault(x => x.id == id);
            if (product != null)
            {
                product.price /= 2;
                context.SaveChanges();
            }
        }

        //全データの取得
        public List<Product> GetAllProducts()
        {
            return context.Products.ToList();
        }

        //特定のデータの取得
        public Product GetSingleProduct(string name)
        {
            return context.Products.AsNoTracking().FirstOrDefault(x => x.name == name);
        }

        //特定のデータの存在確認 ※Any()は使えません!!
        public bool AnyProducts(string name) {
            return context.Products.AsNoTracking().FirstOrDefault(x => x.name == name) != null;
        }

        //特定のデータの個数
        public int CountProductAlmostOutOfStock()
        {
            return context.Products.Count(x => x.quantity < 10);
        }
    }
}
