﻿using Microsoft.EntityFrameworkCore;

namespace greengrocer
{
    public class GreengrocerContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseMySQL(@"server=localhost;database=mydb;userid=root;sslmode=none;");
    }
}
