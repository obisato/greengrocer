﻿using System;
using System.Collections.Generic;

namespace greengrocer
{
    class Program
    {
        static void Main()
        {
            GreengrocerContext context = new GreengrocerContext();
            using DataOperation dataOpration = new DataOperation(context);
            dataOpration.RemoveProduct(1);
            dataOpration.UpdateProductToHalfPrice(2);
            dataOpration.UpdateProductToHalfPrice(3);

            List<Product> products = dataOpration.GetAllProducts();
            Console.WriteLine("ID\tName    \tPrice\tQuantity");
            foreach (Product product in products)
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", product.id, product.name, product.price, product.quantity);
            }
        }
    }
}
